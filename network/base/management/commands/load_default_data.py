"""Creates initial objects for AntennaType, StationType and StationConfigurationSchema models"""
# pylint: disable=line-too-long
# flake8: noqa

from django.core.management.base import BaseCommand

from network.base.models import AntennaType, StationConfigurationSchema, StationType


class Command(BaseCommand):
    """Django management command to fetch Satellites and Transmitters from SatNOGS DB"""
    help = 'Fetches Satellites and Transmitters from SaTNOGS DB'

    def handle(self, *args, **options):
        if not AntennaType.objects.all():
            AntennaType.objects.bulk_create(
                [
                    AntennaType(name="Dipole"),
                    AntennaType(name="V-Dipole"),
                    AntennaType(name="Discone"),
                    AntennaType(name="Ground Plane"),
                    AntennaType(name="Yagi"),
                    AntennaType(name="Cross Yagi"),
                    AntennaType(name="Helical"),
                    AntennaType(name="Parabolic"),
                    AntennaType(name="Vertical"),
                    AntennaType(name="Turnstile"),
                    AntennaType(name="Quadrafilar"),
                    AntennaType(name="Eggbeater"),
                    AntennaType(name="Lindenblad"),
                    AntennaType(name="Parasitic Lindenblad"),
                    AntennaType(name="Patch"),
                    AntennaType(name="Other Directional"),
                    AntennaType(name="Other Omni-Directional"),
                ]
            )

        station_type = StationType.objects.get_or_create(name='RF')[0]

        basic_schema = {
            "$schema": "https://json-schema.org/draft/2020-12/schema",
            "$id": "https://network.satnogs.org/static/rf_basic_configuration_schema",
            "title": "SatNOGS Basic RF Station Configuration",
            "description": "The basic configuration schema for a SatNOGS RF Station",
            "type": "object",
            "properties": {
                "Location Configuration": {
                    "description": "Configuration related to Station's Location",
                    "type": "object",
                    "properties": {
                        "satnogs_station_elev": {
                            "title": "Station Elevation",
                            "description": "Elevation of the station location in meters. Higher precision of this value increases accuracy of Doppler correction while lower precision increases station location privacy.",
                            "type": "integer",
                            "minimum": -500
                        },
                        "satnogs_station_lat": {
                            "title": "Station Latitude",
                            "description": "Latitude of the station location in decimal degrees. Higher precision of this value increases accuracy of Doppler correction while lower precision increases station location privacy.",
                            "type": "number",
                            "minimum": -90,
                            "maximum": 90
                        },
                        "satnogs_station_lon": {
                            "title": "Station Longitude",
                            "description": "Longitude of the station location decimal degrees. Higher precision of this value increases accuracy of Doppler correction while lower precision increases station location privacy.",
                            "type": "number",
                            "minimum": -180,
                            "maximum": 180
                        }
                    },
                    "required": [
                        "satnogs_station_elev", "satnogs_station_lat", "satnogs_station_lon"
                    ],
                    "additionalProperties": False
                },
                "Soapy Configuration": {
                    "description": "Configuration to set Soapy parameters",
                    "type": "object",
                    "properties": {
                        "satnogs_soapy_rx_device": {
                            "title": "Soapy RX Device",
                            "description": "Soapy RX Device",
                            "type": "string",
                            "maxLength": 40,
                            "default": "driver=rtlsdr"
                        },
                        "satnogs_antenna": {
                            "title": "Soapy RX Device Antenna",
                            "description": "SoapySDR device antenna to use for RX. Valid antennas for attached devices can be queried using SoapySDRUtil --probe.",
                            "type": "string",
                            "maxLength": 40,
                            "default": "RX"
                        },
                        "satnogs_rx_samp_rate": {
                            "title": "Soapy RX Sampling Rate",
                            "description": "SoapySDR device sample rate in samples per second(sps). Valid sample rates for attached devices can be queried using SoapySDRUtil --probe.",
                            "type": "integer",
                            "minimum": 0,
                            "default": 2048000
                        }
                    },
                    "required": [
                        "satnogs_soapy_rx_device",
                        "satnogs_antenna",
                        "satnogs_rx_samp_rate",
                    ],
                    "additionalProperties": False
                }
            },
            "required": ["Location Configuration", "Soapy Configuration"],
            "additionalProperties": False
        }

        try:
            basic_station_configuration_schema = StationConfigurationSchema.objects.get(
                name='Basic Configuration', station_type=station_type
            )
            basic_station_configuration_schema.schema = basic_schema
        except StationConfigurationSchema.DoesNotExist:
            basic_station_configuration_schema = StationConfigurationSchema(
                name='Basic Configuration', station_type=station_type, schema=basic_schema
            )

        basic_station_configuration_schema.save()

        unregistered_schema = {
            "$schema": "https://json-schema.org/draft/2020-12/schema",
            "$id": "https://network.satnogs.org/static/rf_basic_configuration_schema",
            "title": "SatNOGS Unregistered RF Station Configuration",
            "description": "The unregistered configuration schema for a SatNOGS RF Station",
            "type": "object",
            "properties": {
                "Location Configuration": {
                    "description": "Configuration related to Station's Location",
                    "type": "object",
                    "properties": {
                        "satnogs_station_elev": {
                            "title": "Station Elevation",
                            "description": "Elevation of the station location in meters. Higher precision of this value increases accuracy of Doppler correction while lower precision increases station location privacy.",
                            "type": "integer",
                            "minimum": -500
                        },
                        "satnogs_station_lat": {
                            "title": "Station Latitude",
                            "description": "Latitude of the station location in decimal degrees. Higher precision of this value increases accuracy of Doppler correction while lower precision increases station location privacy.",
                            "type": "number",
                            "minimum": -90,
                            "maximum": 90
                        },
                        "satnogs_station_lon": {
                            "title": "Station Longitude",
                            "description": "Longitude of the station location decimal degrees. Higher precision of this value increases accuracy of Doppler correction while lower precision increases station location privacy.",
                            "type": "number",
                            "minimum": -180,
                            "maximum": 180
                        }
                    },
                    "required": [
                        "satnogs_station_elev", "satnogs_station_lat", "satnogs_station_lon"
                    ],
                    "additionalProperties": False
                },
            },
            "required": ["Location Configuration"],
            "additionalProperties": False
        }

        try:
            unregistered_station_configuration_schema = StationConfigurationSchema.objects.get(
                name='Unregistered Configuration', station_type=station_type
            )
            unregistered_station_configuration_schema.schema = unregistered_schema
        except StationConfigurationSchema.DoesNotExist:
            unregistered_station_configuration_schema = StationConfigurationSchema(
                name='Unregistered Configuration',
                station_type=station_type,
                schema=unregistered_schema
            )

        unregistered_station_configuration_schema.save()

        advanced_schema = {
            "$schema": "https://json-schema.org/draft/2020-12/schema",
            "$id": "https://network.satnogs.org/static/rf_advanced_configuration_schema",
            "title": "SatNOGS Advanced RF Station Configuration",
            "description": "The advanced configuration schema for a SatNOGS RF Station",
            "type": "object",
            "properties": {
                "Location Configuration": {
                    "description": "Configuration related to Station's Location",
                    "type": "object",
                    "properties": {
                        "satnogs_station_elev": {
                            "title": "Station Elevation",
                            "description": "Elevation of the station location in meters. Higher precision of this value increases accuracy of Doppler correction while lower precision increases station location privacy.",
                            "type": "integer",
                            "minimum": -500
                        },
                        "satnogs_station_lat": {
                            "title": "Station Latitude",
                            "description": "Latitude of the station location. Higher precision of this value increases accuracy of Doppler correction while lower precision increases station location privacy.",
                            "type": "number",
                            "minimum": -90,
                            "maximum": 90
                        },
                        "satnogs_station_lon": {
                            "title": "Station Longitude",
                            "description": "Longitude of the station location. Higher precision of this value increases accuracy of Doppler correction while lower precision increases station location privacy.",
                            "type": "number",
                            "minimum": -180,
                            "maximum": 180
                        }
                    },
                    "additionalProperties": False
                },
                "Soapy Configuration": {
                    "description": "Configuration to set Soapy parameters",
                    "type": "object",
                    "properties": {
                        "satnogs_soapy_rx_device": {
                            "title": "Soapy RX Device",
                            "description": "Soapy RX Device",
                            "type": "string",
                            "maxLength": 40,
                            "default": "driver=rtlsdr"
                        },
                        "satnogs_antenna": {
                            "title": "Soapy RX Device Antenna",
                            "description": "SoapySDR device antenna to use for RX. Valid antennas for attached devices can be queried using SoapySDRUtil --probe.",
                            "type": "string",
                            "maxLength": 40,
                            "default": "RX"
                        },
                        "satnogs_rx_samp_rate": {
                            "title": "Soapy RX Sampling Rate",
                            "description": "SSoapySDR device sample rate in samples per second(sps). Valid sample rates for attached devices can be queried using SoapySDRUtil --probe.",
                            "type": "integer",
                            "minimum": 0,
                            "default": 2048000
                        }
                    },
                    "additionalProperties": False
                },
                "Network": {
                    "description": "Network settings",
                    "type": "object",
                    "properties": {
                        "satnogs_network_api_query_interval": {
                            "title": "API query interval",
                            "description": "Interval (in seconds) for pulling jobs from SatNOGS Network API.",
                            "type": "integer",
                            "minimum": 1,
                            "default": 60
                        },
                        "satnogs_network_api_post_interval": {
                            "title": "API POST request interval",
                            "description": "Interval (in seconds) for posting observation data to SatNOGS Network API.",
                            "type": "integer",
                            "minimum": 1,
                            "default": 180
                        },
                        "satnogs_verify_ssl": {
                            "title": "Verify SSL",
                            "description": "Verify SSL certificates for HTTPS requests.",
                            "type": "boolean",
                            "format": "checkbox",
                            "default": True
                        }
                    },
                    "additionalProperties": False
                },
                "Radio": {
                    "description": "Radio Settings",
                    "type": "object",
                    "properties": {
                        "satnogs_rig_ip": {
                            "title": "Rig IP",
                            "description": "Hostname or IP address of Hamlib rotctld.",
                            "type": "string",
                            "default": "127.0.0.1"
                        },
                        "satnogs_rig_port": {
                            "title": "Rig port",
                            "description": "Hamlib rigctld TCP port.",
                            "type": "integer",
                            "minimum": 1,
                            "maximum": 65535,
                            "default": 4532
                        },
                        "satnogs_doppler_corr_per_sec": {
                            "title": "Doppler corrections per sec",
                            "description": "Number of Doppler corrections per second requested by SatNOGS Radio.",
                            "type": "integer",
                            "minimum": 0
                        },
                        "satnogs_ppm_error": {
                            "title": "Frequency correction",
                            "description": "SoapySDR device oscillator frequency error correction to apply. This setting is defined in parts per million.",
                            "type": "number",
                            "minimum": 0
                        },
                        "satnogs_lo_offset": {
                            "title": "Local oscillator offset",
                            "description": "SoapySDR device local oscillator offset to apply. This setting is used to shift the carrier away from the DC spike. Flowgraph-defined by default.",
                            "type": "integer",
                            "minimum": 0
                        },
                        "satnogs_gain_mode": {
                            "title": "SatNOGS Radio Gain mode",
                            "type": "string",
                            "default": "Overall",
                            "enum": ["Overall", "Specific", "Settings Field"]
                        },
                        "satnogs_rf_gain": {
                            "title": "Soapy RX Device RF Gain",
                            "description": "SoapySDR device overall gain, in dB. Device drivers set individual, device specific gains to approximate linearity on the overall gain.",
                            "type": "number",
                            "default": 32.8
                        },
                        "satnogs_rx_bandwidth": {
                            "title": "SatNOGS Radio RF Bandwidth",
                            "description": "SoapySDR device RF bandwidth. This setting configures the RF filter on devices that support it. Flowgraph-defined by default.",
                            "type": "integer",
                            "minimum": 0
                        },
                        "satnogs_dev_args": {
                            "title": "SoapySDR device arguments",
                            "description": " Valid device arguments for attached devices can be queried using SoapySDRUtil --probe. Flowgraph-defined by default.",
                            "type": "string"
                        },
                        "satnogs_stream_args": {
                            "title": "SoapySDR stream arguments",
                            "description": "Valid stream arguments for attached devices can be queried using SoapySDRUtil --probe. Flowgraph-defined by default.",
                            "type": "string"
                        },
                        "satnogs_tune_args": {
                            "title": "SoapySDR channel tune arguments",
                            "description": "Flowgraph-defined by default.",
                            "type": "integer"
                        },
                        "satnogs_other_settings": {
                            "title": "Other settings",
                            "description": "SoapySDR channel other settings. Flowgraph-defined by default.",
                            "type": "string"
                        },
                        "satnogs_dc_removal": {
                            "title": "Enable automatic DC removal",
                            "type": "boolean",
                            "format": "checkbox",
                            "default": True,
                            "description": "SoapySDR device automatic DC offset suppression. Flowgraph-defined by default."
                        },
                        "satnogs_bb_freq": {
                            "title": "SatNOGS Radio baseband frequency correction",
                            "type": "string",
                            "description": "SoapySDR device baseband CORDIC frequency for devices that support it. Flowgraph-defined by default."
                        },
                        "enable_iq_dump": {
                            "title": "Enable IQ dump",
                            "type": "boolean",
                            "format": "checkbox",
                            "default": False,
                            "description": "Create I/Q data dumps for every observation. Use this feature with caution. Enabling this setting will store large amount of data on the filesystem."
                        },
                        "iq_dump_filename": {
                            "title": "IQ dump filename",
                            "type": "string",
                            "description": "Path to file for storing I/Q data dumps."
                        },
                        "disable_decoded_data": {
                            "title": "Disable decoded data",
                            "type": "boolean",
                            "format": "checkbox",
                            "default": False,
                            "description": "Disable output of decoded data."
                        },
                        "udp_dump_host": {
                            "title": "Destination host for UDP sink",
                            "type": "string",
                            "description": "IP destination of UDP data with Doppler corrected I/Q. Flowgraph-defined by default."
                        },
                        "udp_dump_port": {
                            "title": "UDP dump port",
                            "type": "integer",
                            "minimum": 1,
                            "maximum": 65535,
                            "default": 57356,
                            "description": "Port for UDP data with Doppler corrected I/Q."
                        }
                    },
                    "additionalProperties": False
                },
                "Rotator": {
                    "description": "Rotator settings",
                    "type": "object",
                    "properties": {
                        "satnogs_rot_enabled": {
                            "title": "Enable use of antenna rotator",
                            "description": "Enable SatNOGS Client to connect to an antenna rotator using the Hamlib library.",
                            "type": "boolean",
                            "format": "checkbox",
                            "default": True
                        },
                        "satnogs_rot_model": {
                            "title": "Hamlib rotator model",
                            "type": "string",
                            "default": "ROT_MODEL_DUMMY",
                            "description": "Rotator model to control. This value must be the model string of a Hamlib rotator."
                        },
                        "satnogs_rot_baud": {
                            "title": "Hamlib rotator baud rate",
                            "type": "integer",
                            "default": 19200,
                            "minimum": 0,
                            "description": "Hamlib rotator serial interface baud rate."
                        },
                        "satnogs_rot_port": {
                            "title": "Hamlib rotctld port",
                            "type": "string",
                            "default": "/dev/ttyUSB0",
                            "description": "Path to Hamlib rotator serial port device. The device must be accessible to the user which SatNOGS Client is running."
                        },
                        "satnogs_rot_threshold": {
                            "title": "Hamlib rotator command threshold",
                            "type": "integer",
                            "default": 4,
                            "description": "Azimuth/elevation threshold for moving the rotator. Position changes below this threshold will not cause the rotator to move."
                        },
                        "satnogs_rot_flip": {
                            "title": "Enable Hamlib rotator flip",
                            "type": "boolean",
                            "format": "checkbox",
                            "default": False,
                            "description": "Enable rotator flipping during high elevation passes."
                        },
                        "satnogs_rot_flip_angle": {
                            "title": "Hamlib rotator flip angle",
                            "type": "integer",
                            "default": 75,
                            "minimum": 0,
                            "maximum": 90,
                            "description": "Elevation angle above which the rotator will flip."
                        }
                    },
                    "additionalProperties": False
                },
                "Waterfall": {
                    "description": "Waterfall settings",
                    "type": "object",
                    "properties": {
                        "satnogs_waterfall_autorange": {
                            "title": "Waterfall auto-range",
                            "description": "Automatically set power level range of waterfall images.",
                            "type": "boolean",
                            "default": True,
                            "format": "checkbox"
                        },
                        "satnogs_waterfall_min_value": {
                            "title": "Waterfall minimum power",
                            "type": "integer",
                            "default": -100,
                            "description": "Minimum power level of waterfall images."
                        },
                        "satnogs_waterfall_max_value": {
                            "title": "Waterfall maximum power",
                            "type": "integer",
                            "default": -50,
                            "description": "Maximum power level of waterfall images."
                        }
                    },
                    "additionalProperties": False
                },
                "Artifacts": {
                    "description": "Artifacts settings",
                    "type": "object",
                    "properties": {
                        "satnogs_artifacts_enabled": {
                            "title": "Enable uploading of artifacts",
                            "type": "boolean",
                            "format": "checkbox",
                            "default": False,
                            "description": "Enable generation and uploading of HDF5 artifacts files to SatNOGS DB."
                        },
                        "satnogs_artifacts_api_url": {
                            "title": "Artifacts server API URL",
                            "type": "string",
                            "format": "uri",
                            "default": "https://db.satnogs.org/api/",
                            "description": "URL pointing to API of SatNOGS DB for uploading artifacts."
                        },
                        "satnogs_artifacts_api_post_interval": {
                            "title": "Artifacts server API post interval",
                            "type": "integer",
                            "minimum": 1,
                            "default": 180,
                            "description": "Interval (in seconds) for posting artifacts to SatNOGS DB."
                        },
                        "satnogs_artifacts_api_token": {
                            "title": "Artifacts server API token",
                            "type": "string",
                            "description": "SatNOGS DB API token associated with an account in SatNOGS DB. This token is secret. It is used to upload artifacts to SatNOGS DB. It can be found in SatNOGS DB user page."
                        },
                        "satnogs_keep_artifacts": {
                            "title": "Keep artifact files",
                            "type": "boolean",
                            "format": "checkbox",
                            "default": False,
                            "description": "Do not remove artifact files after successfully uploading them."
                        },
                        "satnogs_artifacts_output_path": {
                            "title": "Path for storing artifact files",
                            "type": "string",
                            "default": "/tmp/.satnogs/artifacts"
                        }
                    },
                    "additionalProperties": False
                },
                "Uploads": {
                    "description": "Upload settings",
                    "type": "object",
                    "properties": {
                        "satnogs_upload_audio_files": {
                            "title": "Enable uploading audio to SatNOGS network",
                            "type": "boolean",
                            "format": "checkbox",
                            "default": True,
                            "description": "Enable/Disable uploading audio files to SatNOGS network."
                        },
                        "satnogs_upload_waterfall_files": {
                            "title": "Enable uploading waterfalls to SatNOGS network",
                            "type": "boolean",
                            "format": "checkbox",
                            "default": True,
                            "description": "Enable/Disable uploading waterfalls to SatNOGS network."
                        }
                    },
                    "additionalProperties": False
                },
                "Scripts": {
                    "description": "Pre/post-observation scripts",
                    "type": "object",
                    "properties": {
                        "satnogs_pre_observation_script": {
                            "title": "Pre-observation script",
                            "type": "string",
                            "description": "A path to an executable to be executed before an observation job is started. Execution of this script blocks the observation job."
                        },
                        "satnogs_post_observation_script": {
                            "title": "Post-observation script",
                            "type": "string",
                            "description": "A path to an executable to be executed after an observation job has finished. Execution of this script blocks the completion of an observation job."
                        }
                    },
                    "additionalProperties": False
                },
                "Paths": {
                    "description": "Path settings",
                    "type": "object",
                    "properties": {
                        "satnogs_app_path": {
                            "title": "App data path",
                            "type": "string",
                            "default": "/tmp/.satnogs",
                            "description": "Base path for storing output files."
                        },
                        "satnogs_output_path": {
                            "title": "Output data path",
                            "type": "string",
                            "default": "/tmp/.satnogs/data",
                            "description": "Base path for storing output files. Output files are: Audio recordings, Waterfall Images, Decoded frames. SatNOGS Artifacts are handled seperately."
                        },
                        "satnogs_complete_output_path": {
                            "title": "Completed data path",
                            "type": "string",
                            "description": "Path to move output files once they are completed. If set to empty, output files are deleted (default). Make sure to have enough storage available as the selected folder will continuously grow (about 30MB for a 5 minute observation)."
                        },
                        "satnogs_incomplete_output_path": {
                            "title": "Incomplete data path",
                            "type": "string",
                            "default": "/tmp/.satnogs/data/incomplete",
                            "description": "Path for moving incomplete output files."
                        },
                        "satnogs_remove_raw_files": {
                            "title": "Remove raw files",
                            "type": "boolean",
                            "format": "checkbox",
                            "default": True,
                            "description": "Remove raw data files used for generating waterfalls."
                        }
                    },
                    "additionalProperties": False
                },
                "Hamlib": {
                    "description": "Hamlib settings.",
                    "type": "object",
                    "properties": {
                        "hamlib_utils_rot_enabled": {
                            "title": "Enable Hamlib rotctld",
                            "type": "boolean",
                            "format": "checkbox",
                            "default": True
                        },
                        "hamlib_utils_rot_opts": {
                            "title": "Hamlib rotctld options",
                            "type": "string",
                            "description": "Enter options for Hamlib rotctld configuration."
                        },
                        "hamlib_utils_rig_disabled": {
                            "title": "Disable Hamlib rigctld",
                            "type": "boolean",
                            "format": "checkbox",
                            "default": False
                        },
                        "hamlib_utils_rig_opts": {
                            "title": "Hamlib rigctld options",
                            "type": "string",
                            "default": "-T 127.0.0.1 -m 1",
                            "description": "Specify initial options for Hamlib rigctld."
                        }
                    },
                    "additionalProperties": False
                },
                "SNMP": {
                    "description": "SNMP settings",
                    "type": "object",
                    "properties": {
                        "snmpd_enabled": {
                            "title": "Enable snmpd",
                            "type": "boolean",
                            "format": "checkbox",
                            "default": False
                        },
                        "snmpd_agentaddress": {
                            "title": "snmpd agentAddress",
                            "type": "string",
                            "description": "Specify the agent address for snmpd."
                        },
                        "snmpd_rocommunity": {
                            "title": "nmpd rocommunity",
                            "type": "string"
                        }
                    },
                    "additionalProperties": False
                },
                "GPS": {
                    "description": "GPS settings",
                    "type": "object",
                    "properties": {
                        "gpsd_enabled": {
                            "title": "Enable GPSd",
                            "type": "boolean",
                            "format": "checkbox",
                            "default": False
                        },
                        "satnogs_gpsd_client_enabled": {
                            "title": "Enable GPSd client",
                            "type": "boolean",
                            "format": "checkbox",
                            "default": False,
                            "description": "Enable SatNOGS Client to connect to a GPSd daemon to pull positional information. The position is queried once, during SatNOGS Client startup."
                        },
                        "satnogs_gpsd_host": {
                            "title": "GPSd host",
                            "type": "string",
                            "default": "127.0.0.1",
                            "description": "Hostname or IP address of GPSd to connect to for pulling positional information."
                        },
                        "satnogs_gpsd_port": {
                            "title": "GPSd port",
                            "type": "integer",
                            "default": 2947,
                            "minimum": 1,
                            "maximum": 65535,
                            "description": "Port of GPSd to connect to for pulling positional information."
                        },
                        "satnogs_gpsd_timeout": {
                            "title": "GPSd timeout",
                            "type": "integer",
                            "minimum": 0,
                            "default": 30,
                            "description": "Time to wait until GPSd returns positional information. A value of 0 means to wait indefinitely."
                        }
                    },
                    "additionalProperties": False
                },
                "Software": {
                    "description": "Software package settings",
                    "type": "object",
                    "properties": {
                        "experimental": {
                            "title": "Experimental",
                            "type": "boolean",
                            "format": "checkbox",
                            "default": False,
                            "description": "Install the latest versions of all software."
                        },
                        "apt_repository": {
                            "title": "Package repository",
                            "type": "string"
                        },
                        "apt_key_url": {
                            "title": "Package repository key URL",
                            "type": "string",
                            "format": "uri"
                        },
                        "apt_key_id": {
                            "title": "Package repository key ID",
                            "type": "string"
                        },
                        "satnogs_client_version": {
                            "title": "SatNOGS client version",
                            "type": "string"
                        },
                        "satnogs_client_url": {
                            "title": "SatNOGS client Git URL",
                            "type": "string",
                            "format": "uri"
                        },
                        "satnogs_radio_flowgraphs_version": {
                            "title": "satnogs-flowgraphs package version",
                            "type": "string",
                            "description": "Specify the version of the satnogs-flowgraphs package."
                        },
                        "satnogs_setup_ansible_url": {
                            "title": "Ansible Git URL",
                            "type": "string",
                            "format": "uri"
                        },
                        "satnogs_setup_ansible_branch": {
                            "title": "Ansible Git branch",
                            "type": "string"
                        },
                        "satnogs_setup_satnogs_config_version": {
                            "title": "SatNOGS Config version",
                            "type": "string"
                        },
                        "satnogs_setup_satnogs_config_url": {
                            "title": "SatNOGS Config Git URL",
                            "type": "string",
                            "format": "uri"
                        }
                    },
                    "additionalProperties": False
                },
                "Debug": {
                    "description": "Debug settings",
                    "type": "object",
                    "properties": {
                        "satnogs_log_level": {
                            "title": "SatNOGS client log level",
                            "type": "string",
                            "default": "WARNING",
                            "enum": ["CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG"],
                            "description": "SatNOGS Client logging level."
                        },
                        "satnogs_scheduler_log_level": {
                            "title": "SatNOGS client scheduler log level",
                            "type": "string",
                            "default": "WARNING",
                            "enum": ["CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG"],
                            "description": "SatNOGS Client scheduler logging level."
                        },
                        "sentry_dsn": {
                            "title": "SatNOGS client Sentry DSN",
                            "type": "string",
                            "default": "d50342fb75aa8f3945e2f846b77a0cdb7c7d2275",
                            "description": "Sentry Data Source Name used for sending events to application monitoring and error tracking server."
                        },
                        "sentry_enabled": {
                            "title": "Enable SatNOGS client Sentry error monitoring",
                            "type": "boolean",
                            "format": "checkbox",
                            "default": False,
                            "description": "Enable sending events to Sentry application monitoring and error tracking server."
                        }
                    },
                    "additionalProperties": False
                }
            },
            "required": ["Location Configuration", "Soapy Configuration"],
            "additionalProperties": False
        }

        try:
            advanced_configuration_schema = StationConfigurationSchema.objects.get(
                name='Advanced Configuration', station_type=station_type
            )
            advanced_configuration_schema.schema = advanced_schema
        except StationConfigurationSchema.DoesNotExist:
            advanced_configuration_schema = StationConfigurationSchema(
                name='Advanced Configuration', station_type=station_type, schema=advanced_schema
            )

        advanced_configuration_schema.save()
