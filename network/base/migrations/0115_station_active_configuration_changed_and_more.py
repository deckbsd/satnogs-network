# Generated by Django 4.2.13 on 2024-07-10 10:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0114_change_station_configuration_schemas'),
    ]

    operations = [
        migrations.AddField(
            model_name='station',
            name='active_configuration_changed',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='stationconfiguration',
            name='applied',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
